
Bonjour,

Je sollicite votre attention sur ma candidature au poste de développeur web.

Titulaire du titre professionnel de développeur logiciel (niveau III) obtenu au terme d’un parcours de reconversion, j’ai occupé durant l’année 2019 les fonctions de développeuse web et de formatrice junior dans le cadre de deux contrats parallèles pour les sociétés Acolad et Simplon.

En tant que développeuse, j’ai travaillé sous environnement Unix avec Drupal 8 et Gatsby.js (générateur de site statique basé sur React) sur la réalisation de sites vitrine dans le respect des bonnes pratiques SEO, accessibilité et RGPD. Ci-dessous deux exemples de mes réalisations :
- Site de la société Acolad développé avec Gatsby.js : https://www.acolad.fr (intégration réalisée "from scratch" avec Sass sans framework CSS.)
- Site de la compagnie de danse Akli développé avec Drupal 8 à partir d'un thème Bootstrap : https://akli.fr

En tant que formatrice junior, mon rôle était centré sur l’initiation des apprenants aux concepts fondamentaux des technologies web front-end (HTML, CSS, JavaScript) et les frameworks et technologies associées (React.js, Vue.js, Webpack, Sass).

Actuellement spécialisée sur React.js, je suis en mesure de me former rapidement sur d’autres technos ou langages. La rigueur, la curiosité et le goût des échanges sont au coeur de ma motivation quotidienne : j’ai à coeur de livrer un code conforme aux bonnes pratiques, testable et documenté. Je maintiens une veille quotidienne pour suivre l’évolution des langages et des outils et approfondir ma compréhension des concepts fondamentaux comme la programmation fonctionnelle ou objet. Autonome et réactive, je me passionne pour les projets qui me sont confiés et je sais aussi être force de proposition.

J’ai aujourd’hui envie d’un projet qui me permette de m’intégrer durablement à une équipe et de me spécialiser sur dans l’écosystème JavaScript qui constitue mon domaine de prédilection ; c’est pourquoi le poste que vous proposez m’intéresse vivement. Ma  carrière antérieure dans le monde de l’édition m'a permis de forger une méthode de travail solide ainsi que l’habitude de communiquer avec de multiples intervenants et je suis à l’aise avec les méthodes agiles, que j’ai pratiquées chez Simplon. Malgré mon profil junior, je pense être en mesure de vous démontrer ma capacité à monter rapidement en compétences. 

Si vous souhaitez en savoir plus, je serai ravie d’échanger avec vous pour plus d’informations.

Cordialement,

Célia Chazel