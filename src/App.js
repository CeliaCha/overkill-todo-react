import React from "react"
import { Route, BrowserRouter as Router } from "react-router-dom"
import { Container } from "@material-ui/core"
import Header from "./components/Header"
import Home from "./views/Home"
import Detail from "./views/Detail"

export const App = () => {
  return (
    <>
      <Container maxWidth="sm">
        <Router>
          <Header />
          <Route exact path="/" component={Home} />
          <Route path="/todo/:id" component={Detail} />
        </Router>
      </Container>
    </>
  )
}

export default App
