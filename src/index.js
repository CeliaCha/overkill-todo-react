/* eslint-disable import/first */

import React from "react"
import { render } from "react-dom"
import { Provider } from "react-redux"
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles"
import store from "./store/storeConfig"
import App from "./App"

// Material UI theme :
const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#f57c00",
      contrastText: "#ffffff"
    },
    secondary: {
      main: "#424242"
    }
  }
})

render(
  <MuiThemeProvider theme={theme}>
    <Provider store={store}>
      <App />
    </Provider>
  </MuiThemeProvider>,
  document.getElementById("root")
)
