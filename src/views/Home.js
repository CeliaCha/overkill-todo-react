import React from "react"
import PropTypes from "prop-types"
import { useLocation } from "react-router-dom"
import { Button } from "@material-ui/core"
import { connect } from "react-redux"
import TodoList from "../components/TodoList"
import TodoModal from "../components/TodoModal"
import { toggleModal, navigate } from "../store/user_interface/actions"
import { getTodoTasks, getDoneTasks } from "../store/selectors"

const mapStateToProps = state => ({
  modal: state.ui.home.modal,
  tasks: state.todos,
  todoTasks: getTodoTasks(state),
  doneTasks: getDoneTasks(state)
})

export const Home = ({ modal, todoTasks, doneTasks, dispatchToggleModal, dispatchNavigate }) => {
  // const todoTasks = tasks.filter(task => !task.done).sort((a, b) => b.id - a.id)
  // const doneTasks = tasks.filter(task => task.done).sort((a, b) => b.doneAt - a.doneAt)
  // dispatchNavigate(useLocation().pathname)
  return (
    <>
      <Button variant="outlined" color="primary" onClick={() => dispatchToggleModal("Add")}>
        Add todo
      </Button>
      <TodoList tasks={todoTasks} />
      <TodoList tasks={doneTasks} />
      {modal && <TodoModal />}
    </>
  )
}

Home.propTypes = {
  modal: PropTypes.string.isRequired,
  dispatchToggleModal: PropTypes.func.isRequired,
  dispatchNavigate: PropTypes.func.isRequired
}

export default connect(mapStateToProps, { dispatchToggleModal: toggleModal, dispatchNavigate: navigate })(Home)
