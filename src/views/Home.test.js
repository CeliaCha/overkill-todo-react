import React from "react"
import { shallow } from "enzyme"
// import renderer from "react-test-renderer"
// eslint-disable-next-line import/no-extraneous-dependencies
import routeData from "react-router"
// import configureStore from "redux-mock-store"
// import { Provider } from "react-redux"
import { Button } from "@material-ui/core"
// import { preloadedState } from "../store/storeConfig"
import { Home } from "./Home"

const mockLocation = { pathname: "/" }
const props = { dispatchToggleModal: jest.fn(), dispatchNavigate: jest.fn(), modal: "" }

beforeEach(() => {
  jest.spyOn(routeData, "useLocation").mockReturnValue(mockLocation)
})

describe("Rendering", () => {
  it("should render without crashing", () => {
    shallow(<Home {...props} />)
  })
})

describe("Add Todo button", () => {
  it("should call toggleModal with 'add' type on click", () => {
    const button = shallow(<Home {...props} />).find(Button)
    button.simulate("click")
    expect(props.dispatchToggleModal).toHaveBeenCalledWith("Add")
  })
})
