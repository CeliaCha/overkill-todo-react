import React from "react"
import PropTypes from "prop-types"
import { Link, useLocation } from "react-router-dom"
import { connect } from "react-redux"
import { navigate } from "../store/user_interface/actions"

const mapStateToProps = (state, { match: { params } }) => ({
  todo: state.todos.find(el => el.id.toString() === params.id)
})

export const Detail = ({ todo, dispatchNavigate }) => {
  dispatchNavigate(useLocation().pathname)
  const createdAt = new Date(todo.createdAt).toLocaleDateString("fr-FR")
  const doneAt = new Date(todo.doneAt).toLocaleDateString("fr-FR")
  return (
    <>
      <p>Created : {createdAt}</p>
      <p>{todo.done ? `Completed: ${doneAt}` : "Not completed yet"}</p>
      <h1>{todo.title}</h1>
      <p>{todo.description}</p>
      <p />
      <Link to="/">Home</Link>
    </>
  )
}

Detail.propTypes = {
  todo: PropTypes.shape({
    id: PropTypes.number.isRequired,
    createdAt: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    description: PropTypes.string,
    done: PropTypes.bool.isRequired,
    doneAt: PropTypes.number.isRequired
  }).isRequired,
  dispatchNavigate: PropTypes.func.isRequired
}

export default connect(mapStateToProps, { dispatchNavigate: navigate })(Detail)
