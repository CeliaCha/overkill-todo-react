import React from "react"
import { shallow } from "enzyme"
import renderer from "react-test-renderer"
import routeData, { MemoryRouter } from "react-router"
import { Detail } from "./Detail"

const mockLocation = { pathname: "/" }
let props = {
  todo: {
    id: 1,
    createdAt: 1578935131760,
    done: false,
    doneAt: 0,
    title: "List my TODOs",
    description: "As a user I would like to list my current todos"
  },
  dispatchNavigate: jest.fn()
}

beforeEach(() => {
  jest.spyOn(routeData, "useLocation").mockReturnValue(mockLocation)
})

describe("Rendering", () => {
  it("should render without crashing", () => {
    shallow(<Detail {...props} />)
  })
})

describe("Not completed todo", () => {
  it("should match snapshot", () => {
    const render = renderer
      .create(
        <MemoryRouter>
          <Detail {...props} />
        </MemoryRouter>
      )
      .toJSON()
    expect(render).toMatchSnapshot()
  })
})

describe("Completed todo", () => {
  it("should match snapshot", () => {
    props = { ...props, todo: { ...props.todo, done: true, doneAt: 1580405870686 } }
    const render = renderer
      .create(
        <MemoryRouter>
          <Detail {...props} />
        </MemoryRouter>
      )
      .toJSON()
    expect(render).toMatchSnapshot()
  })
})
