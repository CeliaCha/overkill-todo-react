const getPageTitle = location => (location.includes("todo") ? "Todo detail" : "My List")
export default getPageTitle
