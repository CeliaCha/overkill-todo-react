/* eslint-disable import/no-named-as-default */
/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable react/jsx-props-no-spreading */
import React from "react"
import renderer from "react-test-renderer"
import { Typography } from "@material-ui/core"
import { shallow, mount } from "enzyme"
import configureStore from "redux-mock-store"
import { Provider } from "react-redux"
import TodoList, { TodoList as TodoListComponent } from "./TodoList"

const mockStore = configureStore([])

const items = [
  {
    id: 1,
    createdAt: 1578935131760,
    done: false,
    doneAt: 0,
    title: "List my TODOs",
    description: "As a user I would like to list my current todos"
  }
]

describe("TodoList", () => {
  it("should render", () => {
    const component = shallow(<TodoListComponent items={items} />)
  })

  it("should match snapshot", () => {
    const render = renderer.create(<TodoListComponent items={items} />)
    expect(render.toJSON()).toMatchSnapshot()
    console.log(render.instance())
  })
  it.skip("should render as many items as provided in props", () => {
    const render = renderer.create(<TodoListComponent />).toJSON()
    expect(render).toMatchSnapshot()
  })
})
