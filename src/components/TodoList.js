import React from "react"
import PropTypes from "prop-types"
import { connect } from "react-redux"
import { List, Divider } from "@material-ui/core"
import TodoItem from "./TodoItem"

// const mapStateToProps = state => ({
//   items: state.todos

//   // todos: state.todos.filter(todo => !todo.done).sort((a, b) => b.id - a.id),
//   // dones: state.todos.filter(todo => todo.done).sort((a, b) => b.doneAt - a.doneAt)
// })

export const TodoList = ({ tasks }) => {
  // Sort list items from last to first by creation date (id) / done date
  // const todos = items.filter(todo => !todo.done).sort((a, b) => b.id - a.id)
  // const dones = items.filter(todo => todo.done).sort((a, b) => b.doneAt - a.doneAt)
  // console.log(tasks)
  return (
    <>
      <List>
        {tasks.map(el => (
          <TodoItem item={el} key={el.id} />
        ))}
      </List>
      {/* <Divider />
      <List>
        {dones.map(el => (
          <TodoItem item={el} key={el.id} />
        ))}
      </List> */}
    </>
  )
}

const todosPropTypes = PropTypes.arrayOf(
  PropTypes.shape({
    id: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    done: PropTypes.bool.isRequired
  }).isRequired
)

TodoList.propTypes = {
  tasks: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      title: PropTypes.string.isRequired,
      done: PropTypes.bool.isRequired
    }).isRequired
  ).isRequired
}

// export default connect(mapStateToProps)(TodoList)
export default TodoList
