import React from "react"
import renderer from "react-test-renderer"
import { Typography } from "@material-ui/core"
import { shallow } from "enzyme"
import { Header as TestHeader } from "./Header"

describe("Rendering", () => {
  it("should render without crashing", () => {
    shallow(<TestHeader pageName="home" />)
  })
  it("should match snapshot", () => {
    const render = renderer.create(<TestHeader pageName="home" />).toJSON()
    expect(render).toMatchSnapshot()
  })
})

describe("Title", () => {
  it("should display page name", () => {
    const homeName = "My List"
    const detailName = "Todo detail"
    const homeTitle = shallow(<TestHeader pageName={homeName} />)
      .find(Typography)
      .text()
    expect(homeTitle).toMatch(homeName)
    const detailTitle = shallow(<TestHeader pageName={detailName} />)
      .find(Typography)
      .text()
    expect(detailTitle).toMatch(detailName)
  })
})
