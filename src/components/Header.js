import React from "react"
import PropTypes from "prop-types"
import { withRouter } from "react-router-dom"
import { AppBar, Toolbar, Typography } from "@material-ui/core"
// import { connect } from "react-redux"

// const mapStateToProps = state => ({
//   pageName: state.ui.path.includes("todo") ? "Todo detail" : "My List"
// })

export const Header = ({ location: { pathname } }) => {
  console.log(pathname)
  return (
    <header>
      <AppBar position="static">
        <Toolbar>
          <Typography edge="center" variant="h6">
            Overkill-todo-react | {pathname}
          </Typography>
        </Toolbar>
      </AppBar>
    </header>
  )
}

// Header.propTypes = {
//   pageName: PropTypes.string.isRequired
// }

export default withRouter(Header)
