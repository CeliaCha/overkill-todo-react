import React, { useState } from "react"
import PropTypes from "prop-types"
import { TextField, Dialog, DialogActions, DialogContent, DialogTitle, Button } from "@material-ui/core"
import { connect } from "react-redux"
import { createTodo, updateTodo } from "../store/todos/actions"
import { toggleModal } from "../store/user_interface/actions"

const TITLE_MAX_LENGTH = 50
const DESCR_MAX_LENGTH = 500
const HELPER_TITLE = `Maximum characters allowed : ${TITLE_MAX_LENGTH}`
const HELPER_DESCR = `Maximum characters allowed : ${DESCR_MAX_LENGTH}`

const mapStateToProps = state => {
  const editedTodo = state.todos.find(el => el.id === state.ui.home.editedTodo_id) // undefined if there is no edited todo
  return {
    preset: {
      // Define preset props depending on modal type :
      type: state.ui.home.modal,
      id: editedTodo?.id || state.todos.lastId + 1, // optional chaining ES2019 syntax
      presetTitle: editedTodo?.title || "",
      presetDescription: editedTodo?.description || "",
      formTitle: editedTodo ? "Edit todo" : "New todo",
      submitText: editedTodo ? "Update" : "Create"
    }
  }
}

const TodoModal = ({ preset, dispatchCreate, dispatchUpdate, dispatchToggleModal }) => {
  const { type, id, presetTitle, presetDescription, formTitle, submitText } = preset
  const [title, setTitle] = useState(presetTitle)
  console.log(useState)
  const [description, setDescription] = useState(presetDescription)
  const payload = [id, title, description]

  // Form validation :
  const error = { title: title.length > TITLE_MAX_LENGTH, description: description.length > DESCR_MAX_LENGTH }
  const submitAllowed =
    title && !error.title && !error.description && (title !== presetTitle || description !== presetDescription)

  return (
    <Dialog open aria-labelledby="form-dialog-title">
      <DialogTitle id="form-dialog-title">{formTitle}</DialogTitle>
      <DialogContent>
        <TextField
          autoFocus
          required
          error={error.title}
          helperText={error.title && HELPER_TITLE}
          variant="outlined"
          margin="normal"
          label="Title"
          type="text"
          defaultValue={title}
          fullWidth
          onChange={e => setTitle(e.target.value)}
        />
        <TextField
          multiline
          error={error.description}
          helperText={error.description && HELPER_DESCR}
          variant="outlined"
          rows="8"
          margin="normal"
          label="Description"
          type="text"
          defaultValue={description}
          fullWidth
          onChange={e => setDescription(e.target.value)}
        />
      </DialogContent>
      <DialogActions>
        <Button color="primary" onClick={() => dispatchToggleModal("")}>
          Cancel
        </Button>
        <Button
          color="primary"
          disabled={!submitAllowed}
          onClick={() => (type === "Add" ? dispatchCreate(...payload) : dispatchUpdate(...payload))}
        >
          {submitText}
        </Button>
      </DialogActions>
    </Dialog>
  )
}

TodoModal.propTypes = {
  preset: PropTypes.shape({
    type: PropTypes.string.isRequired,
    id: PropTypes.number.isRequired,
    presetTitle: PropTypes.string.isRequired,
    presetDescription: PropTypes.string.isRequired,
    formTitle: PropTypes.string.isRequired,
    submitText: PropTypes.string.isRequired
  }).isRequired,
  dispatchCreate: PropTypes.func.isRequired,
  dispatchUpdate: PropTypes.func.isRequired,
  dispatchToggleModal: PropTypes.func.isRequired
}

export default connect(mapStateToProps, {
  dispatchCreate: createTodo,
  dispatchUpdate: updateTodo,
  dispatchToggleModal: toggleModal
})(TodoModal)
