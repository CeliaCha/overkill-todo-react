import React from "react"
import PropTypes from "prop-types"
import { connect } from "react-redux"
import { ListItem, ListItemIcon, ListItemText, Checkbox, Icon, Link } from "@material-ui/core"
import { makeStyles } from "@material-ui/core/styles"
import { Link as RouterLink } from "react-router-dom"
import { toggleTodo, deleteTodo } from "../store/todos/actions"
import { toggleModal } from "../store/user_interface/actions"

const useStyles = makeStyles({
  itemDefault: {
    textDecoration: "none"
  },
  itemDone: {
    textDecoration: "line-through"
  }
})

export const TodoItem = ({ item, dispatchToggleModal, dispatchToggleTodo, dispatchDeleteTodo }) => {
  const styles = useStyles()
  return (
    <ListItem key={item.id} dense>
      <ListItemIcon>
        <Checkbox checked={item.done} onChange={() => dispatchToggleTodo(item.id)} />
      </ListItemIcon>
      <ListItemText id={item.id} primary={item.title} className={item.done ? styles.itemDone : styles.itemDefault} />
      <Link component={RouterLink} to={`/todo/${item.id}`}>
        See more
      </Link>
      <ListItemIcon>
        <Icon onClick={() => dispatchToggleModal("Edit", item.id)}>edit</Icon>
      </ListItemIcon>
      <ListItemIcon>
        <Icon onClick={() => dispatchDeleteTodo(item.id)}>delete_forever</Icon>
      </ListItemIcon>
    </ListItem>
  )
}

TodoItem.propTypes = {
  item: PropTypes.shape({
    id: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    done: PropTypes.bool.isRequired
  }).isRequired,
  dispatchToggleTodo: PropTypes.func.isRequired,
  dispatchDeleteTodo: PropTypes.func.isRequired,
  dispatchToggleModal: PropTypes.func.isRequired
}

export default connect(null, {
  dispatchToggleTodo: toggleTodo,
  dispatchToggleModal: toggleModal,
  dispatchDeleteTodo: deleteTodo
})(TodoItem)
