import React from "react"
import ReactDOM from "react-dom"
import { Provider } from "react-redux"
import { BrowserRouter as Router } from "react-router-dom"
import store from "../store/storeConfig"

import TodoItem from "./TodoItem"

const todo = {
  id: 7,
  createdAt: 1578937731940,
  done: false,
  doneAt: 0,
  title: "Add Material Components",
  description: "To keep the UI simple, the use of Material components is highly recommended"
}

it("renders without crashing", () => {
  const div = document.createElement("div")
  ReactDOM.render(
    <Provider store={store}>
      <Router>
        <TodoItem item={todo} />
      </Router>
    </Provider>,
    div
  )
})
