import { CREATE_TODO, UPDATE_TODO, DELETE_TODO, TOGGLE_TODO } from "../todos/types"

export default () => {
  return next => action => {
    const todos = JSON.parse(localStorage.getItem("todos"))

    switch (action.type) {
      case CREATE_TODO:
        todos.push(action.payload)
        localStorage.setItem("todos", JSON.stringify(todos))
        localStorage.setItem("lastId", JSON.stringify(action.payload.id))
        break
      case TOGGLE_TODO: {
        localStorage.setItem(
          "todos",
          JSON.stringify(
            todos.map(todo => {
              if (todo.id === action.payload.id) {
                return {
                  ...todo,
                  done: !todo.done,
                  doneAt: !todo.doneAt ? action.payload.timestamp : 0
                }
              }
              return todo
            })
          )
        )
        break
      }
      case UPDATE_TODO: {
        localStorage.setItem(
          "todos",
          JSON.stringify(
            todos.map(todo => {
              if (todo.id === action.payload.id) {
                return {
                  ...todo,
                  title: action.payload.title,
                  description: action.payload.description
                }
              }
              return todo
            })
          )
        )
        break
      }
      case DELETE_TODO:
        localStorage.setItem("todos", JSON.stringify(todos.filter(todo => todo.id !== action.payload.id)))
        break
      default: // Do nothing
    }
    next(action)
  }
}
