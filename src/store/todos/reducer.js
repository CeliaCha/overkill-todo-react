import { CREATE_TODO, UPDATE_TODO, DELETE_TODO, TOGGLE_TODO } from "./types"

export const getTodoTasks = state => state.todos.filter(task => !task.done).sort((a, b) => b.id - a.id)

export const getDoneTasks = state => state.filter(task => task.done).sort((a, b) => b.doneAt - a.doneAt)

// const getBelts = (state) => state.shop.items.belts;

const todos = (state = null, action) => {
  switch (action.type) {
    case CREATE_TODO: {
      const newState = [...state, action.payload]
      newState.lastId = state.lastId + 1
      return newState
    }
    case UPDATE_TODO:
      return state.map(todo =>
        todo.id === action.payload.id
          ? {
              ...todo,
              title: action.payload.title,
              description: action.payload.description
            }
          : todo
      )
    case TOGGLE_TODO:
      return state.map(todo =>
        todo.id === action.payload.id
          ? {
              ...todo,
              done: !todo.done,
              doneAt: !todo.doneAt ? action.payload.timestamp : 0
            }
          : todo
      )
    case DELETE_TODO:
      return state.filter(todo => todo.id !== action.payload.id)
    default:
      return state
  }
}
export default todos
