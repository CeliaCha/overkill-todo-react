import { CREATE_TODO, UPDATE_TODO, DELETE_TODO, TOGGLE_TODO } from "./types"

const createTodo = (id, title, description) => {
  return {
    type: CREATE_TODO,
    payload: {
      id,
      createdAt: Date.now(),
      done: false,
      doneAt: null,
      title,
      description
    }
  }
}

const updateTodo = (id, title, description) => ({
  type: UPDATE_TODO,
  payload: {
    id,
    title,
    description
  }
})

const deleteTodo = id => ({
  type: DELETE_TODO,
  payload: {
    id
  }
})

const toggleTodo = id => ({
  type: TOGGLE_TODO,
  payload: {
    id,
    timestamp: Date.now() // Used only if toogle-done
  }
})

export { createTodo, updateTodo, deleteTodo, toggleTodo }
