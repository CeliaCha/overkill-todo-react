import { combineReducers } from "redux"
import todos from "./todos/reducer"
import ui from "./user_interface/reducer"

const rootReducer = combineReducers({ todos, ui })

export default rootReducer
