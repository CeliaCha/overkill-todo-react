export const getTaskSortedById = state => state.todos.sort((a, b) => b.id - a.id)
export const getTaskSortedByDoneAt = state => state.todos.sort((a, b) => b.doneAt - a.doneAt)

export const getTodoTasks = state => state.todos.filter(task => !task.done).sort((a, b) => b.id - a.id)
export const getDoneTasks = state => state.todos.filter(task => task.done).sort((a, b) => b.doneAt - a.doneAt)
