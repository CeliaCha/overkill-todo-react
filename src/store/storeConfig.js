import { createStore, applyMiddleware } from "redux"
import rootReducer from "./rootReducer"
import LocalStorage from "./middlewares/localstorage"
import todosJson from "../data/todos.json"

// Initialize localStorage with JSON data on first load :
if (!localStorage.length) {
  localStorage.setItem("todos", JSON.stringify(todosJson))
  localStorage.setItem("lastId", todosJson.length)
}

// Affect localStorage data to preloaded state :
const todos = JSON.parse(localStorage.getItem("todos"))
todos.lastId = JSON.parse(localStorage.getItem("lastId"))
const ui = {
  path: "home",
  home: {
    modal: "", // empty string means no modal ; other values are "Add"/"Edit"
    editedTodo_id: 0 // 0 means no todo edited (number is required for PropTypes)
  }
}

const preloadedState = { todos, ui }

const store = createStore(rootReducer, preloadedState, applyMiddleware(LocalStorage))

export { preloadedState } // imported in tests files
export default store
