import { NAVIGATE, TOGGLE_MODAL } from "./types"
import { CREATE_TODO, UPDATE_TODO } from "../todos/types"

// const initialState = {
//   path: "home",
//   home: {
//     modal: "", // empty string means no modal ; other values are "Add"/"Edit"
//     editedTodo_id: 0 // 0 means no todo edited (number is required for PropTypes)
//   }
// }

const userInterface = (state = null, action) => {
  switch (action.type) {
    case NAVIGATE:
      return { ...state, path: action.payload.path }
    case TOGGLE_MODAL: {
      return { ...state, home: { modal: action.payload.modal, editedTodo_id: action.payload.todoId } }
    }
    case CREATE_TODO:
    case UPDATE_TODO:
      return { ...state, home: { modal: "", editedTodo_id: 0 } } // reset home state after action is completed
    default:
      return state
  }
}

export default userInterface
