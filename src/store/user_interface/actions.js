import { NAVIGATE, TOGGLE_MODAL } from "./types"

const navigate = path => ({
  type: NAVIGATE,
  payload: {
    path
  }
})

const toggleModal = (modal, todoId = 0) => ({
  type: TOGGLE_MODAL,
  payload: {
    modal,
    todoId
  }
})

export { navigate, toggleModal }
